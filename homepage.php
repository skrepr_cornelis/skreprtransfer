<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Transfer</title>
    <meta name="description" content="Transfer">
    <meta name="author" content="SitePoint">
    <link rel="stylesheet" href="style/css/style.css">
    <link rel="stylesheet" href="style/css/stylesheet.scss">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link id="favicon" rel="shortcut icon" type="image/png" href="favicon.png" />
</head>

<body>
<header class="header text-white d-flex" style="background-image: url(https://skrepr.com/img/header.png);background-size:cover;background-position:center center;">
    <div class="container my-auto">
        <img width="112" height="30" src="https://skrepr.com/img/skrepr-dia.svg">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1>Skrepr Transfer<span class="cursor white">_</span></h1>
            </div>
        </div>
    </div>
</header>
    <?php include 'index.php';?>
<div class="footer">
    <div class="container">
        <div class="col-6 d-flex">
            <h1>SkreprTransfer</h1>
        </div>
    </div>
</div>
</body>
</html>